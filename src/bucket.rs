use super::node::*;
use std::collections::*;

pub struct Bucket<Id, Addr> {
    pub nodes: VecDeque<Node<Id, Addr>>,
}
