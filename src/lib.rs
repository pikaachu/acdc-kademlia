#![allow(dead_code)]
mod bucket;
mod node;

use bucket::Bucket;
use node::Node;

struct KBucketTable<Id, Addr> {
    me: Node<Id, Addr>,
    peers: Vec<Bucket<Id, Addr>>,
}

impl<Id, Addr> KBucketTable<Id, Addr> {
    pub fn new(my_id: Id, my_address: Addr) -> Self {
        Self { 
            me: Node::new(my_id, my_address),
            peers: vec![],
        }
    }

    pub fn add_peer(&mut self, peer: Node<Id, Addr>) {
        unimplemented!();
    }

    pub fn remove_peer(&mut self, peer: Node<Id, Addr>) {
        unimplemented!();
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
