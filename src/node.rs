pub struct Node<Id, Addr> {
    pub id: Id,
    pub addr: Addr,
}

impl<Id, Addr> Node<Id, Addr> {
    pub fn new(id: Id, addr: Addr) -> Self {
        Self { 
            id: id,
            addr: addr
        }
    }
}
